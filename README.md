To Start building invertify

    repo init -u https://gitlab.com/invertifyrecovery/manifest.git -b master
    
To initialize a shallow clone, which will save even more space, use a command like this:

    repo init --depth=1 -u https://gitlab.com/invertifyrecovery/manifest.git -b master

Then to sync up:

    repo sync

Then to build:

     cd <source-dir>; export ALLOW_MISSING_DEPENDENCIES=true; . build/envsetup.sh; lunch omni_<device>-eng; mka recoveryimage

